## 2022-06-11

- Audio
  - Audio Players
    - Added [Lofi](https://github.com/dvx/lofi)
  - Audio Editors
    - Added [Kid3](https://kid3.kde.org/)
  - Others
    - Added [AudioPlaybackConnector](https://github.com/ysc3839/AudioPlaybackConnector)
- Graphics
  - Others
    - Added [Pensela](https://github.com/weiameili/Pensela)
- Browsers and Extensions
  - Extensions
    - Added [FastForward](https://github.com/FastForwardTeam/FastForward)
- Downloader
  - Added [Open Video Downloader](https://github.com/jely2002/youtube-dl-gui)
- File Sharing
  - Added [libcimbar](https://cimbar.org/)
- Productivity
  - File Search
    - Added [Pinpoint](https://github.com/dkgv/pinpoint)
  - Automation
    - Added [SikuliX](https://github.com/RaiMan/SikuliX1)
    - Added [Macro Deck](https://macrodeck.org/)
- Personal Finance
  - Added [Budget Badger](https://budgetbadger.io/)
- Personalization
  - Added [Mica for Everyone](https://github.com/MicaForEveryone/MicaForEveryone)
- Network and Admin
  - Remote Desktop
    - Added [Sunshine](https://github.com/loki-47-6F-64/sunshine)
- Utilities
  - Others
    - Added [Nyrna](https://github.com/Merrit/nyrna)

## 2022-06-03

- Audio
  - Audio Editors
    - Added [Mp3tag](https://mp3tag.de/en)
- Graphics
  - Screenshot and Screen Recorder
    - Added [LICEcap](https://www.cockos.com/licecap/)
    - Added [gifcap](https://gifcap.dev/)
  - Image Editors
    - Added [Pixelitor](https://pixelitor.sourceforge.io/)
- Browsers and Extensions
  - Extensions
    - Added [BetterViewer](https://github.com/Ademking/BetterViewer)

## 2022-05-27

- Audio
  - Others
    - Added [SpleeterGUI](https://github.com/boy1dr/SpleeterGui)
  - Audio Players
    - Removed [Google Play Music™ Desktop Player](https://github.com/MarshallOfSound/Google-Play-Music-Desktop-Player-UNOFFICIAL-)
- Video
  - Others
    - Added [QPrompt](https://qprompt.app/)
  - Video Players
    - Removed [pogu.live](https://github.com/Alissonsleal/TwitchSubVod)
- Graphics
  - 2D and 3D
    - Added [Enve](https://github.com/MaurycyLiebner/enve)
  - Others
    - Added [Capture2text](https://sourceforge.net/projects/capture2text/)
    - Added [NormCap](https://github.com/dynobo/normcap/)
- Games
  - Added [Rigs of Rods](https://www.rigsofrods.org/)
  - Added [OpenTTD](https://www.openttd.org/)
- Productivity
  - Writing Tools
    - Added [ghostwriter](https://github.com/wereturtle/ghostwriter)
  - Time Tracking
    - Added [Tockler](https://github.com/MayGo/tockler)
  - File Search
    - Added [grepWin](https://github.com/stefankueng/grepWin)
    - Added [dnGrep](https://github.com/dnGrep/dnGrep)
  - Markdown Editors
    - Removed [Notable](https://github.com/notable/notable)
- Health and Fitness
    - Added [OpenStreetMap](https://www.openstreetmap.org/)
- Development
  - Added [Atom](https://github.com/atom/atom)
  - Added [WindTerm](https://github.com/kingToolbox/WindTerm)
- Utilities
  - Others
    - Added [Ten Hands](https://github.com/saisandeepvaddi/ten-hands)
    - Added [workspacer](https://github.com/workspacer/workspacer)
